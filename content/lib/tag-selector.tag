<tag-selector>
	{opts.inspect}
	<ul class="menu">
		<li each="{ options }" no-reorder
		     onclick="{ parent.selectItem }"
				 class="menu__item { 'menu__item--active': selected, 'menu__item--disabled': disabled, 'menu__item--hover': active }">
			{ text }
		</li>
	</ul>
	<div class="buttons">
		<div class="button" onclick="{ selectAll }">select all</div>
		<div class="button" onclick="{ selectNone }">clear</div>
	</div>
	<p></p>
	<script>
			this.on('mount', () => {
				options = opts.tags.options;
				// document.addEventListener('click', handleClickOutside)
				this.update()
	      opts.trigger('mounted',this)
			})

			this.selectAll = function(e){
				opts.tags.options.forEach(function(i){
					i.selected = true;
				});
				this.update()
				this.trigger('select');
				this.trigger('interaction',e);
			}

			this.selectNone = function(e){
				opts.tags.options.forEach(function(i){
					i.selected = false;
				});
				this.update();
				this.trigger('select',e);
			}

			this.selectItem = (e,direct) => {
				if(opts.tags.multi == false) {
					opts.tags.options.forEach(function(i){
						i.selected = false;
					});
				}
				e.item.selected = !e.item.selected

				this.update()
				this.trigger('select')
			}

			this.selectBy = (key,values,direct) => {
				values = [].concat.apply([], [values])
				opts.tags.options.forEach(function(i){
					i.selected = values.includes(i[key]);
				});
				this.update();
				this.trigger('select');
			}
	</script>

	<style scoped>
	.buttons {
	    display: flex;
	    zoom: .8;
	}

	</style>
</tag-selector>
