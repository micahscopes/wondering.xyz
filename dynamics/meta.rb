preSiteMeta = @in+"/meta.yaml"
metaDeps = []
metaDeps << preSiteMeta if File.exist?(preSiteMeta)
file SITEMETA => metaDeps do
  m = meta(SITEMETA)
  m << meta(preSiteMeta) if File.exist?(preSiteMeta)
  m << {"posts"=>[],"site"=>{}}
  FileList[@out+"/**/*.meta.yaml"].each do |srcMeta|
    sm = meta(srcMeta)
    m["posts"]<<sm
  end
  m["posts"] = m["posts"].sort{|e,f| f["date"].to_i <=> e["date"].to_i}
  tags = m["posts"].collect{|e| e["tags"]}.flatten.uniq
  tags.delete(nil)
  tags.delete("")
  m["tags"] = tags
end
CLEAN.include SITEMETA
task :meta => SITEMETA

file @navigator do
  #  sh "echo ''>>#{@navigator}"
  sh "touch #{@navigator}"
end
navigator = "#{@out}/tags/index.html"
tagsDir = "#{@out}/tags/"
cocoon(tagsDir)
file navigator => [tagsDir,SITEMETA,@navigator] do
  sitemeta = meta(SITEMETA)
  navpage = Mustache.render( File.read(@navigator), {"site"=>sitemeta.root_hash,"posts"=>sitemeta["posts"].inspect})
  File.write(navigator,navpage)
end
task :navigator => navigator

mainFeed = "#{@out}/feed.xml"
CLOBBER.include mainFeed
task :feed => [SITEMETA] do
  m = meta(SITEMETA)
  tags = m["tags"]
  # puts m
  tags.each do |tag|
    tagFeed = tagsDir+tag+".xml"
    file tagFeed => [SITEMETA,tagsDir] do
      before =  m["posts"]
      haveTag = m["posts"].find_all do |p|
        p['tags'] ? p['tags'].map{|t| t.to_url}.include?(tag) : false
      end

      feed = Mustache.render( File.read(@feed), {'title'=>"#{tag} feed",'posts'=>haveTag,'site'=>m.root_hash})
      File.write(tagFeed, feed)
    end
    Rake::Task[tagFeed].invoke
  end

  file mainFeed => [SITEMETA,@feed] do
    feed = Mustache.render(File.read(@feed),{'posts'=>m["posts"].inspect,'site'=>m.root_hash})
    File.write(mainFeed, feed)
  end
  Rake::Task[mainFeed].invoke
end
