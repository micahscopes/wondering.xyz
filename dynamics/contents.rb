def toHTML(files,template_src=@theme,&blk)
  @transplant.with files do |frm,to|
    result = to.pathmap("%X.html")
    src = File.read(frm)
    # puts src
    srcmetafile = to+".meta.yaml"
    cocoon srcmetafile # make sure the parent directory is set up
    metaDeps = [frm]
    if File.exists? frm+".meta.yaml"
      metaDeps << frm+".meta.yaml"
    end
    if File.exists? File.dirname(frm)+"/meta.yaml"
      metaDeps << File.dirname(frm)+"/meta.yaml"
    end

    file srcmetafile => metaDeps do
      srcmeta = Meta.about frm, :path =>srcmetafile, :data_key => "source_body"
      if File.exists? frm+".meta.yaml"
        srcmeta ** meta(frm+".meta.yaml")
      end
      if File.exists? File.dirname(frm)+"/meta.yaml"
        srcmeta ** meta(File.dirname(frm)+"/meta.yaml")
      end
      CLEAN.include srcmeta
      srcmeta.transaction do |m|
        m["content"] = {}
        m["uri"] = @makeUri.with(frm)[0]
        blk[frm, to, src, m]
        m["date_raw"] = m["date"] if m["date"]
        m["date"] = Chronic.parse m["date"] if m["date"] rescue m["date"]
        m["date_fancy"] = m["date"].strftime("%a, %d %b %Y") if m["date"] rescue m["date"]
        m["updated"] = Chronic.parse m["updated"] if m["updated"]
        if m["tags"]
          m["tags"].map!{|t| t.strip}
          # puts info["tags"].inspect
          m["tags"].delete(nil)
          m["tags"].delete("")
          m["original_tags"] = m["tags"].clone
          m["original_tags"].map! do |t|
            {t=>t.to_url}
          end
          m["tags"].map!{|t| t.to_url}
        end
      end
    end
    file SITEMETA => srcmetafile
    file result => [srcmetafile,template_src,SITEMETA] do
      sitemeta = meta(SITEMETA)
      srcmeta = meta(srcmetafile)
      tmp_src = srcmeta["template"] != nil ? srcmeta["template"] : template_src
      template = File.read(tmp_src)
      themed = Mustache.render(template,srcmeta.knit({"site"=>sitemeta.to_h}))
      File.write(result,themed)
    end
    CLOBBER.include [result,srcmetafile]
  end
end

toHTML(FileList["#{@in}/**/*.html"],@theme) do |frm,to,src,m|
  src = "<!DOCTYPE html>\n"+src
  h = Nokogiri.parse(src)
  m["title"] = h.css("title").remove.inner_text
  m["links_to"] = h.xpath("//a").collect{|a|a.attribute("href").value}.uniq
  m["date"] = h.xpath("//time").inner_html
  m["summary"] = h.css("summary").inner_html if h.css("summary")
  mathnb = h.css('#notebook-container')[0]
  if mathnb
    m["content"]["main"] = mathnb.inner_html
    m["tags"]
    # m["tags"] << "jupyter-notebook"
    m["mathnb"] = true
  else
    m["content"]["main"] = h.css("body").inner_html
    m["content"]["head"] = h.css("head").inner_html
  end
  m["content"]["navigation"] = !m["links_to"].empty? || !m["tags"].to_s.strip.empty? ? m["links_to"].collect{|uri|"<li><a href=#{uri}>#{uri}</a></li>"}.join : nil
end

toHTML(FileList["#{@in}/**/*.md"],@theme) do |frm,to,src,m|
  m["content"]["main"] = RDiscount.new(m["source_body"]).to_html rescue ""
  m["content"]["head"] = ""
  h = Nokogiri.parse("<!DOCTYPE html>"+m["content"]["main"].to_s)
  m["content"]["summary"] = h.css("summary").inner_html if h.css("summary")
  m["links_to"] = h.xpath("//a").collect{|a|a.attribute("href").value}.uniq
  m["content"]["navigation"] = !m["links_to"].empty? || !m["tags"].to_s.strip.empty? ? m["links_to"].collect{|uri|"<li><a href=#{uri}>#{uri}</a></li>"}.join : nil
end

@transplant.with FileList["#{@in}/**/*.scss"] do |frm,to|
  result = to.pathmap("%X%{^.scss,.css}x")
  file result => [frm] do |result|
    sh "compass compile --sass-dir #{@in} --css-dir #{@out} --force"
  end
  CLOBBER.include(result)
end
